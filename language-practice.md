"Запрещенные слова" это игра для практики иностранных языков.

* Кто-нибудь задаёт "запрещенные слова". Сообщение со списком слов закрепляется в основной телеграм-группе компании 

  * Эти слова нельзя произносить по-русски (здесь и далее подразумевается, что правила распрастраняются на однокоренные и синонимы слова)
  * Можно произносить на любом языке кроме русского
  * Само предложение, в котором слово участвует, можно произносить по-русски, но желательно тоже на иностранном языке
  * Исключения когда слово можно произносить по-русски:
  
    * в контексте обсуждения перевода (например кто-нибудь спрашивает в чем отличие hi и hello)
    * при общении с внешними людьми (те, у кого нет доступа к основной группе)

* Символ нарушителя

  * человек, который произнес слово из списка по-русски, получает на свой стол переходящий символ нарушителя
  * после обнуления списка слов, символ нарушителя переставляется на нейтральное место

* Обновление слов:

  * обновить слова можно после 8 утра
  * Если текущий набор слов было быть активно в предыдущем рабочем дне и его никто его не произнес на русском, то список слов обнуляется и задаётся новое слово. В противном случае можно только добавить слово к существующему списку

    * Добавленное слово должно быть в начале списке чтобы его было сразу видно
    * Формат сообщения со списком слов: 
    
       :construction: *Новое слово* (*синонимы, однокоренные*) + *вчерашнее слово* (*синонимы, однокоренные*) + ... + *самое первое слово* (*синонимы, однокоренные*)
